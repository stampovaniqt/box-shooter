﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        // if user running game on web client hide quit button because when clicked nothing will happen
        if (Application.platform == RuntimePlatform.WebGLPlayer)
        {
            GameObject.Find("QuitButton").SetActive(false);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
