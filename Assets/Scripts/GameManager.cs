﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Net.Sockets;
using UnityEngine.SceneManagement;
using System.Globalization;

public class GameManager : MonoBehaviour
{

    // make game manager public static so can access this from other scripts
    public static GameManager gm;

    // public variables
    public int score = 0;

    public bool canBeatLevel = false;
    public int beatLevelScore = 20;

    public float startTime = 5.0f; //set in the game 

    public Text mainScoreDisplay;
    public Text mainTimerDisplay;

    public GameObject gameOverScoreOutline;

    public AudioSource musicAudioSource;

    public bool gameIsOver = false;

    public GameObject playAgainButtons;
    public string playAgainLevelToLoad;

    public GameObject nextLevelButtons;
    public string nextLevelToLoad;

    private float currentTime;

    // reference to log file
    private LoggerCSV logger;
    //private float lastLoggingTime;

    // for sending log files over server
    //[SerializeField] private int portNum;

    //[SerializeField] private string hostName;

    //SocketClient client = new SocketClient();
    //bool endOfCalibrationPeriod = false;
    //bool hasSocketConnection = false;

    // setup the game
    void Start()
    {
        //try
        //{
        //    client = new SocketClient();
        //    client.StartSocketClient(portNum,hostName);
        //    hasSocketConnection = true;
        //}
        //catch (SocketException ex)
        //{
        //    hasSocketConnection = false;
        //    Debug.Log("The socket connection can't be established.");
        //}

        //string socp = (string)client.SendMessageToSocketServer("SOCP");

        // set the current time to the startTime specified
        currentTime = startTime;
        //lastLoggingTime = startTime;

        // get a reference to the GameManager component for use by other scripts
        if (gm == null)
            gm = this.gameObject.GetComponent<GameManager>();

        // init scoreboard to 0
        mainScoreDisplay.text = "0";

        // inactivate the gameOverScoreOutline gameObject, if it is set
        if (gameOverScoreOutline)
            gameOverScoreOutline.SetActive(false);

        // inactivate the playAgainButtons gameObject, if it is set
        if (playAgainButtons)
            playAgainButtons.SetActive(false);

        // inactivate the nextLevelButtons gameObject, if it is set
        if (nextLevelButtons)
            nextLevelButtons.SetActive(false);

        logger = GameObject.Find("LoggerCSV").GetComponent<LoggerCSV>();
        if (logger == null)
        {
            Debug.Log("Unable to set reference to LoggerCSV.");
        }
        else
        {
            logger.Log("New game for user with email: " + PlayerPrefs.GetString("email"));
        }

    }

    // this is the main game event loop
    void Update()
    {
        if (!gameIsOver)
        {
            if (canBeatLevel && (score >= beatLevelScore))
            {
                // check to see if beat game
                BeatLevel();
            }
            else if (currentTime < 0)
            {
                // check to see if timer has run out
                EndGame();
            }
            else
            {
                // game playing state, so update the timer
                currentTime -= Time.deltaTime;
                mainTimerDisplay.text = currentTime.ToString("0.00");
                
                // on update to log the results of player
                LogPlayerResult();
            }
        }
    }

    void EndGame()
    {
        // game is over
        gameIsOver = true;

        // repurpose the timer to display a message to the player
        mainTimerDisplay.text = "GAME OVER";

        // activate the gameOverScoreOutline gameObject, if it is set 
        if (gameOverScoreOutline)
            gameOverScoreOutline.SetActive(true);

        // activate the playAgainButtons gameObject, if it is set 
        if (playAgainButtons)
            playAgainButtons.SetActive(true);

        // reduce the pitch of the background music, if it is set 
        if (musicAudioSource)
            musicAudioSource.pitch = 0.5f; // slow down the music
    }

    void BeatLevel()
    {
        // game is over
        gameIsOver = true;

        // repurpose the timer to display a message to the player
        mainTimerDisplay.text = "LEVEL COMPLETE";

        // activate the gameOverScoreOutline gameObject, if it is set 
        if (gameOverScoreOutline)
            gameOverScoreOutline.SetActive(true);

        // activate the nextLevelButtons gameObject, if it is set 
        if (nextLevelButtons)
            nextLevelButtons.SetActive(true);

        // reduce the pitch of the background music, if it is set 
        if (musicAudioSource)
            musicAudioSource.pitch = 0.5f; // slow down the music
    }

    // public function that can be called to update the score or time
    public void targetHit(int scoreAmount, float timeAmount)
    {
        // increase the score by the scoreAmount and update the text UI
        score += scoreAmount;
        mainScoreDisplay.text = score.ToString();

        // increase the time by the timeAmount
        currentTime += timeAmount;

        // don't let it go negative
        if (currentTime < 0)
            currentTime = 0.0f;

        // update the text UI
        mainTimerDisplay.text = currentTime.ToString("0:00");

        //this is if we want everytime he hits a target to log it
        //LogPlayerResult();
    }

    // public function that can be called to restart the game
    public void RestartGame()
    {
        // we are just loading a scene (or reloading this scene)
        // which is an easy way to restart the level
        //Application.LoadLevel (playAgainLevelToLoad);
        SceneManager.LoadScene(playAgainLevelToLoad);
    }

    // public function that can be called to go to the next level of the game
    public void NextLevel()
    {
        // we are just loading the specified next level (scene)
        //Application.LoadLevel (nextLevelToLoad);
        SceneManager.LoadScene(nextLevelToLoad);
    }

    private void LogPlayerResult()
    {
        //this is for the adaptation part
        //try
        //{
        //    if (hasSocketConnection && endOfCalibrationPeriod)
        //    {
        //        string currentEDA = (string)client.SendMessageToSocketServer("GET_EDA");
        //        Debug.Log("currentEDA=" + currentEDA);
        //        currentEDA = !String.IsNullOrEmpty(currentEDA) ? currentEDA.Substring(currentEDA.IndexOf('{')) : "";
        //        currentEDA = currentEDA.Replace("NaN", "0");
        //    }

        //}
        //catch (SocketException e)
        //{
        //    Debug.Log("The socket connection was aborted");
        //}
        // IMPORTANT WARNING 
        /*
         * If the culture of your computer doesn't accep ',' as delimitor comment bellow code 
         */
        NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;
        var formattedCurrentTime = currentTime.ToString("F3", nfi);

        // IMPORTANT WARNING 
        logger.Log(String.Format("{0}, {1}, {2},", score, formattedCurrentTime, playAgainLevelToLoad));
    }

}
