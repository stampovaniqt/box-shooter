﻿using System;
using System.Net.Sockets;
using System.Text;
using UnityEngine;

public class SocketClient : MonoBehaviour
{

    private int portNum; // = 10116;
    private string hostName; // = "127.0.0.1";
    private TcpClient tcpClient = new TcpClient();
    private NetworkStream networkStream = null;

    public void StartSocketClient(int portNum, string hostName)
    {
        this.portNum = portNum;
        this.hostName = hostName;
        tcpClient.Connect(hostName, portNum);

        networkStream = tcpClient.GetStream();
        if (networkStream.CanWrite && networkStream.CanRead)
        {
            //Utils.Logger.Log("Start socket connection.");
        }
        else if (!networkStream.CanRead)
        {
            //Utils.Logger.Log("You can not write data to this stream");
            networkStream.Close();
            tcpClient.Close();
        }
        else if (!networkStream.CanWrite)
        {
            // Utils.Logger.Log("You can not read data from this stream");
            networkStream.Close();
            tcpClient.Close();
        }
    }

    public bool IsSocketConnected()
    {
        return tcpClient.Client.IsBound;
    }

    public object SendMessageToSocketServer(String message)
    {
        string returnData = "";
        if (networkStream != null && networkStream.CanWrite && networkStream.CanRead)
        {
            //Utils.Logger.Log("Send to the socket message: " + message);
            Byte[] sendBytes = Encoding.ASCII.GetBytes(message);
            networkStream.Write(sendBytes, 0, sendBytes.Length);

            // Reads the NetworkStream into a byte buffer.
            byte[] bytes = new byte[tcpClient.ReceiveBufferSize];
            int BytesRead = networkStream.Read(bytes, 0, (int) tcpClient.ReceiveBufferSize);

            // Returns the data received from the host to the console.
            returnData = Encoding.ASCII.GetString(bytes, 0, BytesRead);
            //Utils.Logger.Log("Socket answer: \r\n" + returnData);
        }

        return returnData;
    }

    public void CloseSocketConnection()
    {
        networkStream.Close();
        tcpClient.Close();
        //Utils.Logger.Log("Close socket connection.");
    }
}
